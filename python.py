from bottle import route, run, template, request, response, HTTPResponse
import bottle

USERNAME = "ta210"
PASSWORD = "110ta210"


def check(username, password):
    return username == USERNAME and password == PASSWORD


@bottle.route('/')
@bottle.auth_basic(check)
def index():
    return template("templates/login")


@route('/api/login', method='POST')
def login():
    id = request.forms.get('id')
    pw = request.forms.get('pw')
    if id == 'ta210':
        if pw == 'ta210':
            return template("templates/topPage")
        else:
            return template("templates/error")
    else:
        return template("templates/error")

run(host='localhost', port=8080, debug=True)